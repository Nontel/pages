(I, Leah Nielson, am in no way a lawyer and this is not legal advice, please contact a real lawyer if you need legal advice)

# Can we legally reverse engineer Intel cards?

The answer is: Probably not.

## iwlwifi.LICENSE

Intel has (gracefully) given anybody permission to use its proprietary binary firmware for any reason*. You can download the firmware from the linux-firmware repository and use it on your machine with no legal consequences.

*: More on this later.

This firmware is called "iwlwifi", and it has a licensing file. You cannot legally bypass the firmware license, it applies to everyone who has used iwlwifi in any way or form.

If you want to then you can read the license [here](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/LICENCE.iwlwifi_firmware) but TL;DR it gives you a limited patent license, it gives warranty protections to Intel and it allows you to use the proprietary firmware, *except for reverse engineering*

But here's the thing, we don't have to reverse engineer the firmware, we can develop a driver and firmware for Intel branded WiFi chips without ever touching the proprietary firmware, this means we are safe from the license that comes with iwlwifi.

If it means reverse engineering is going to be harder then so be it, we cannot risk our users getting in trouble for using our works.

But there is a much bigger problem and that is intellectual property.

## Patents

If you have read the license then you might have noticed the patent license clause it has, this means that Intel *does* own WiFi patents that are legally enforceable and this is a big problem for us.

If we reverse engineer and ignore the patents then Intel could sue the project, it's developers, the hoster (Codeberg) of the repository and maybe even the users.

Intel has over 8500 patents regarding Wireless technology, most of it is because of Intel's cellular business and those patents do not interfere with the development of Nontel, but there is an unspecified amount of patents that could be endangering this project.

## Solutions?

We cannot reverse engineer Intel's WiFi cards because of the patents, worst case scenario is we get sued by Intel and we lose, but we will most likely get a cease and desist letter (which we have to comply with either way, we do not have enough money to defend Nontel)

We must ask Intel to kindly release it's firmware under an open source license. And no! it cannot be BSD or MIT! It *has* to be either Apache or GPLv2, otherwise we are still not safe from Intel's patents.

There is still hope though, I think Intel would release the firmware or some parts of it because recently OpenBSD (I am sure you have all heard about them ;) ) asked Realtek to re-license it's firmware under a free license* and I am pretty sure Intel will also do it.

*: The firmware was licensed under a BSD license (which doesn't offer much patent protection) and the firmware was obfuscated but we could de-obfuscate the Iwlwifi firmware if it is licensed under a better license such as Apache or GPLv2