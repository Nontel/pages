# Nontel

## Why?

Currently, all* wireless card firmware is nonfree, and there is no solution to this problem. Because many new chips don't have firmwre baked-in, and it is supplied by the user, the chips could potentially be more free than the currently "most free" wireless devices. The goal of this project is to cleanly engineer a free universal drop-in firmware and driver replacement so that wireless cards become free, and anyone can run fully free operating systems.

\* Before you say "What about Atheros?": old Atheros chips are also nonfree, but the firmware is baked into the card and not replacable by the user, making them even more nonfree than cards with firmware supplied by the OS. 

# Discussion

Join us on matrix at [#nontel:kde.org](https://matrix.to/#/#nontel:kde.org) or on the IRC bridge at [#nontel on libera.chat](https://web.libera.chat/#nontel)

# Contribute

This project is going to take a lot of brains and a lot of testing to do legally, so people with expertise in firmware development, or even a nonfree wireless card, can help the project by submitting PRs and just trying the firmware on their card and reporting back. Schematics and datasheets for any cards are greatly appreciated.